import RPi.GPIO as GPIO
import time
import threading
import sys
from flask import Flask, render_template, request, redirect

app = Flask(__name__)

GPIO.setmode(GPIO.BCM)


mixin_defaults = {
    "gin": 3,
    "schweppes": 5,
}
    

# Create a dictionary called pins to store the pin number, name, and pin state:
pins = {
    #23: {"name": "GPIO 23", "state": GPIO.LOW},
    "air_pump": {"pin": 5, "name": "GPIO 5", "default_state": GPIO.LOW},
    "gin": {"pin": 6, "name": "GPIO 6", "default_state": GPIO.LOW},
    "schweppes": {"pin": 13, "name": "GPIO 13", "default_state": GPIO.LOW},
    
}

def reset_pins():
    print("resetting pins")
    for pin_name, pin_info in pins.items():
        pin = pin_info["pin"]
        GPIO.setup(pin, GPIO.OUT)
        GPIO.output(pin, pin_info["default_state"])


@app.route("/")
def main():
    return render_template("main.html", defaults=mixin_defaults)


def progsleep(duration):
    t0 = time.time()
    while time.time() - t0 < duration:
        print('.', end='')
        time.sleep(.1)
        sys.stdout.flush()
    print('%.2f' % duration)


# The function below is executed when someone requests a URL with the pin number and action in it:
@app.route("/", methods=['POST'])
def action():
    mixin_times = mixin_defaults.copy()
    
    for key, value in request.form.items():
        if key in mixin_times:
            mixin_times[key] = min(10, float(value))
    
    reset_pins()
    
    try:
        print("turning pump on!")
        GPIO.output(pins["air_pump"]["pin"], GPIO.HIGH)
        progsleep(2)
        
        for ingredient, itime in sorted(mixin_times.items()):
            if itime <= 0:
                continue
            print("turning %s on!" % ingredient)
            GPIO.output(pins[ingredient]["pin"], GPIO.HIGH)
            progsleep(itime)
            GPIO.output(pins[ingredient]["pin"], GPIO.LOW)
            print("turning %s off!" % ingredient)

        GPIO.output(pins["air_pump"]["pin"], GPIO.LOW)
    finally:
        reset_pins()
    
    return redirect('/')

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80, debug=True)
